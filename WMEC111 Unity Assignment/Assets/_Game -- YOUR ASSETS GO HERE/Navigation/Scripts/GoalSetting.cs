﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoalSetting : MonoBehaviour {

	//create variable placeholder to allow input of gameobject through inspector
	public GameObject GoalObject;
	//establish a empty transform variable named goal
	Transform goal;

	//use this method for intialising
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		//set goal to be the transform of the object we assigned through the inspector
		goal = GoalObject.transform;
		//set the navmesh's goal location
		NavMeshAgent agent = GetComponent<NavMeshAgent>();
		//make the agent/enemy move to the goal position
		agent.destination = goal.position; 

	}
}
